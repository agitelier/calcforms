package com.mycompany.myapp.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mycompany.myapp.web.rest.TestUtil;

public class DecentralizeDecisionMakingTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DecentralizeDecisionMaking.class);
        DecentralizeDecisionMaking decentralizeDecisionMaking1 = new DecentralizeDecisionMaking();
        decentralizeDecisionMaking1.setId("id1");
        DecentralizeDecisionMaking decentralizeDecisionMaking2 = new DecentralizeDecisionMaking();
        decentralizeDecisionMaking2.setId(decentralizeDecisionMaking1.getId());
        assertThat(decentralizeDecisionMaking1).isEqualTo(decentralizeDecisionMaking2);
        decentralizeDecisionMaking2.setId("id2");
        assertThat(decentralizeDecisionMaking1).isNotEqualTo(decentralizeDecisionMaking2);
        decentralizeDecisionMaking1.setId(null);
        assertThat(decentralizeDecisionMaking1).isNotEqualTo(decentralizeDecisionMaking2);
    }
}
