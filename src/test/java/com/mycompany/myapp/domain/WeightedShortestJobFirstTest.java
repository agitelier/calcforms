package com.mycompany.myapp.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mycompany.myapp.web.rest.TestUtil;

public class WeightedShortestJobFirstTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(WeightedShortestJobFirst.class);
        WeightedShortestJobFirst weightedShortestJobFirst1 = new WeightedShortestJobFirst();
        weightedShortestJobFirst1.setId("id1");
        WeightedShortestJobFirst weightedShortestJobFirst2 = new WeightedShortestJobFirst();
        weightedShortestJobFirst2.setId(weightedShortestJobFirst1.getId());
        assertThat(weightedShortestJobFirst1).isEqualTo(weightedShortestJobFirst2);
        weightedShortestJobFirst2.setId("id2");
        assertThat(weightedShortestJobFirst1).isNotEqualTo(weightedShortestJobFirst2);
        weightedShortestJobFirst1.setId(null);
        assertThat(weightedShortestJobFirst1).isNotEqualTo(weightedShortestJobFirst2);
    }
}
