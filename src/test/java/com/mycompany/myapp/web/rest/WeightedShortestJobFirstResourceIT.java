package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.CalcformsApp;
import com.mycompany.myapp.config.TestSecurityConfiguration;
import com.mycompany.myapp.domain.WeightedShortestJobFirst;
import com.mycompany.myapp.repository.WeightedShortestJobFirstRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link WeightedShortestJobFirstResource} REST controller.
 */
@SpringBootTest(classes = { CalcformsApp.class, TestSecurityConfiguration.class })
@AutoConfigureMockMvc
@WithMockUser
public class WeightedShortestJobFirstResourceIT {

    private static final String DEFAULT_GROUP = "AAAAAAAAAA";
    private static final String UPDATED_GROUP = "BBBBBBBBBB";

    private static final String DEFAULT_CLASS_NUM = "AAAAAAAAAA";
    private static final String UPDATED_CLASS_NUM = "BBBBBBBBBB";

    private static final String DEFAULT_FEATURE = "AAAAAAAAAA";
    private static final String UPDATED_FEATURE = "BBBBBBBBBB";

    private static final Integer DEFAULT_USER_BV = 1;
    private static final Integer UPDATED_USER_BV = 2;

    private static final Integer DEFAULT_TIME_C = 1;
    private static final Integer UPDATED_TIME_C = 2;

    private static final Integer DEFAULT_RROE = 1;
    private static final Integer UPDATED_RROE = 2;

    private static final Integer DEFAULT_JOB_S = 1;
    private static final Integer UPDATED_JOB_S = 2;

    @Autowired
    private WeightedShortestJobFirstRepository weightedShortestJobFirstRepository;

    @Autowired
    private MockMvc restWeightedShortestJobFirstMockMvc;

    private WeightedShortestJobFirst weightedShortestJobFirst;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WeightedShortestJobFirst createEntity() {
        WeightedShortestJobFirst weightedShortestJobFirst = new WeightedShortestJobFirst()
            .group(DEFAULT_GROUP)
            .classNum(DEFAULT_CLASS_NUM)
            .feature(DEFAULT_FEATURE)
            .userBV(DEFAULT_USER_BV)
            .timeC(DEFAULT_TIME_C)
            .rroe(DEFAULT_RROE)
            .jobS(DEFAULT_JOB_S);
        return weightedShortestJobFirst;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WeightedShortestJobFirst createUpdatedEntity() {
        WeightedShortestJobFirst weightedShortestJobFirst = new WeightedShortestJobFirst()
            .group(UPDATED_GROUP)
            .classNum(UPDATED_CLASS_NUM)
            .feature(UPDATED_FEATURE)
            .userBV(UPDATED_USER_BV)
            .timeC(UPDATED_TIME_C)
            .rroe(UPDATED_RROE)
            .jobS(UPDATED_JOB_S);
        return weightedShortestJobFirst;
    }

    @BeforeEach
    public void initTest() {
        weightedShortestJobFirstRepository.deleteAll();
        weightedShortestJobFirst = createEntity();
    }

    @Test
    public void createWeightedShortestJobFirst() throws Exception {
        int databaseSizeBeforeCreate = weightedShortestJobFirstRepository.findAll().size();
        // Create the WeightedShortestJobFirst
        restWeightedShortestJobFirstMockMvc.perform(post("/api/weighted-shortest-job-firsts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(weightedShortestJobFirst)))
            .andExpect(status().isCreated());

        // Validate the WeightedShortestJobFirst in the database
        List<WeightedShortestJobFirst> weightedShortestJobFirstList = weightedShortestJobFirstRepository.findAll();
        assertThat(weightedShortestJobFirstList).hasSize(databaseSizeBeforeCreate + 1);
        WeightedShortestJobFirst testWeightedShortestJobFirst = weightedShortestJobFirstList.get(weightedShortestJobFirstList.size() - 1);
        assertThat(testWeightedShortestJobFirst.getGroup()).isEqualTo(DEFAULT_GROUP);
        assertThat(testWeightedShortestJobFirst.getClassNum()).isEqualTo(DEFAULT_CLASS_NUM);
        assertThat(testWeightedShortestJobFirst.getFeature()).isEqualTo(DEFAULT_FEATURE);
        assertThat(testWeightedShortestJobFirst.getUserBV()).isEqualTo(DEFAULT_USER_BV);
        assertThat(testWeightedShortestJobFirst.getTimeC()).isEqualTo(DEFAULT_TIME_C);
        assertThat(testWeightedShortestJobFirst.getRroe()).isEqualTo(DEFAULT_RROE);
        assertThat(testWeightedShortestJobFirst.getJobS()).isEqualTo(DEFAULT_JOB_S);
    }

    @Test
    public void createWeightedShortestJobFirstWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = weightedShortestJobFirstRepository.findAll().size();

        // Create the WeightedShortestJobFirst with an existing ID
        weightedShortestJobFirst.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restWeightedShortestJobFirstMockMvc.perform(post("/api/weighted-shortest-job-firsts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(weightedShortestJobFirst)))
            .andExpect(status().isBadRequest());

        // Validate the WeightedShortestJobFirst in the database
        List<WeightedShortestJobFirst> weightedShortestJobFirstList = weightedShortestJobFirstRepository.findAll();
        assertThat(weightedShortestJobFirstList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllWeightedShortestJobFirsts() throws Exception {
        // Initialize the database
        weightedShortestJobFirstRepository.save(weightedShortestJobFirst);

        // Get all the weightedShortestJobFirstList
        restWeightedShortestJobFirstMockMvc.perform(get("/api/weighted-shortest-job-firsts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(weightedShortestJobFirst.getId())))
            .andExpect(jsonPath("$.[*].group").value(hasItem(DEFAULT_GROUP)))
            .andExpect(jsonPath("$.[*].classNum").value(hasItem(DEFAULT_CLASS_NUM)))
            .andExpect(jsonPath("$.[*].feature").value(hasItem(DEFAULT_FEATURE)))
            .andExpect(jsonPath("$.[*].userBV").value(hasItem(DEFAULT_USER_BV)))
            .andExpect(jsonPath("$.[*].timeC").value(hasItem(DEFAULT_TIME_C)))
            .andExpect(jsonPath("$.[*].rroe").value(hasItem(DEFAULT_RROE)))
            .andExpect(jsonPath("$.[*].jobS").value(hasItem(DEFAULT_JOB_S)));
    }
    
    @Test
    public void getWeightedShortestJobFirst() throws Exception {
        // Initialize the database
        weightedShortestJobFirstRepository.save(weightedShortestJobFirst);

        // Get the weightedShortestJobFirst
        restWeightedShortestJobFirstMockMvc.perform(get("/api/weighted-shortest-job-firsts/{id}", weightedShortestJobFirst.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(weightedShortestJobFirst.getId()))
            .andExpect(jsonPath("$.group").value(DEFAULT_GROUP))
            .andExpect(jsonPath("$.classNum").value(DEFAULT_CLASS_NUM))
            .andExpect(jsonPath("$.feature").value(DEFAULT_FEATURE))
            .andExpect(jsonPath("$.userBV").value(DEFAULT_USER_BV))
            .andExpect(jsonPath("$.timeC").value(DEFAULT_TIME_C))
            .andExpect(jsonPath("$.rroe").value(DEFAULT_RROE))
            .andExpect(jsonPath("$.jobS").value(DEFAULT_JOB_S));
    }
    @Test
    public void getNonExistingWeightedShortestJobFirst() throws Exception {
        // Get the weightedShortestJobFirst
        restWeightedShortestJobFirstMockMvc.perform(get("/api/weighted-shortest-job-firsts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateWeightedShortestJobFirst() throws Exception {
        // Initialize the database
        weightedShortestJobFirstRepository.save(weightedShortestJobFirst);

        int databaseSizeBeforeUpdate = weightedShortestJobFirstRepository.findAll().size();

        // Update the weightedShortestJobFirst
        WeightedShortestJobFirst updatedWeightedShortestJobFirst = weightedShortestJobFirstRepository.findById(weightedShortestJobFirst.getId()).get();
        updatedWeightedShortestJobFirst
            .group(UPDATED_GROUP)
            .classNum(UPDATED_CLASS_NUM)
            .feature(UPDATED_FEATURE)
            .userBV(UPDATED_USER_BV)
            .timeC(UPDATED_TIME_C)
            .rroe(UPDATED_RROE)
            .jobS(UPDATED_JOB_S);

        restWeightedShortestJobFirstMockMvc.perform(put("/api/weighted-shortest-job-firsts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedWeightedShortestJobFirst)))
            .andExpect(status().isOk());

        // Validate the WeightedShortestJobFirst in the database
        List<WeightedShortestJobFirst> weightedShortestJobFirstList = weightedShortestJobFirstRepository.findAll();
        assertThat(weightedShortestJobFirstList).hasSize(databaseSizeBeforeUpdate);
        WeightedShortestJobFirst testWeightedShortestJobFirst = weightedShortestJobFirstList.get(weightedShortestJobFirstList.size() - 1);
        assertThat(testWeightedShortestJobFirst.getGroup()).isEqualTo(UPDATED_GROUP);
        assertThat(testWeightedShortestJobFirst.getClassNum()).isEqualTo(UPDATED_CLASS_NUM);
        assertThat(testWeightedShortestJobFirst.getFeature()).isEqualTo(UPDATED_FEATURE);
        assertThat(testWeightedShortestJobFirst.getUserBV()).isEqualTo(UPDATED_USER_BV);
        assertThat(testWeightedShortestJobFirst.getTimeC()).isEqualTo(UPDATED_TIME_C);
        assertThat(testWeightedShortestJobFirst.getRroe()).isEqualTo(UPDATED_RROE);
        assertThat(testWeightedShortestJobFirst.getJobS()).isEqualTo(UPDATED_JOB_S);
    }

    @Test
    public void updateNonExistingWeightedShortestJobFirst() throws Exception {
        int databaseSizeBeforeUpdate = weightedShortestJobFirstRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWeightedShortestJobFirstMockMvc.perform(put("/api/weighted-shortest-job-firsts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(weightedShortestJobFirst)))
            .andExpect(status().isBadRequest());

        // Validate the WeightedShortestJobFirst in the database
        List<WeightedShortestJobFirst> weightedShortestJobFirstList = weightedShortestJobFirstRepository.findAll();
        assertThat(weightedShortestJobFirstList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteWeightedShortestJobFirst() throws Exception {
        // Initialize the database
        weightedShortestJobFirstRepository.save(weightedShortestJobFirst);

        int databaseSizeBeforeDelete = weightedShortestJobFirstRepository.findAll().size();

        // Delete the weightedShortestJobFirst
        restWeightedShortestJobFirstMockMvc.perform(delete("/api/weighted-shortest-job-firsts/{id}", weightedShortestJobFirst.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<WeightedShortestJobFirst> weightedShortestJobFirstList = weightedShortestJobFirstRepository.findAll();
        assertThat(weightedShortestJobFirstList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
