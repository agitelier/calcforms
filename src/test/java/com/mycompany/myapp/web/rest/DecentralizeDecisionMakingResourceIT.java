package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.CalcformsApp;
import com.mycompany.myapp.config.TestSecurityConfiguration;
import com.mycompany.myapp.domain.DecentralizeDecisionMaking;
import com.mycompany.myapp.repository.DecentralizeDecisionMakingRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DecentralizeDecisionMakingResource} REST controller.
 */
@SpringBootTest(classes = { CalcformsApp.class, TestSecurityConfiguration.class })
@AutoConfigureMockMvc
@WithMockUser
public class DecentralizeDecisionMakingResourceIT {

    private static final String DEFAULT_GROUP = "AAAAAAAAAA";
    private static final String UPDATED_GROUP = "BBBBBBBBBB";

    private static final String DEFAULT_CLASS_NUM = "AAAAAAAAAA";
    private static final String UPDATED_CLASS_NUM = "BBBBBBBBBB";

    private static final String DEFAULT_DECISION = "AAAAAAAAAA";
    private static final String UPDATED_DECISION = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FREQUENT = false;
    private static final Boolean UPDATED_FREQUENT = true;

    private static final Boolean DEFAULT_TIME_C = false;
    private static final Boolean UPDATED_TIME_C = true;

    private static final Boolean DEFAULT_ECONO_S = false;
    private static final Boolean UPDATED_ECONO_S = true;

    @Autowired
    private DecentralizeDecisionMakingRepository decentralizeDecisionMakingRepository;

    @Autowired
    private MockMvc restDecentralizeDecisionMakingMockMvc;

    private DecentralizeDecisionMaking decentralizeDecisionMaking;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DecentralizeDecisionMaking createEntity() {
        DecentralizeDecisionMaking decentralizeDecisionMaking = new DecentralizeDecisionMaking()
            .group(DEFAULT_GROUP)
            .classNum(DEFAULT_CLASS_NUM)
            .decision(DEFAULT_DECISION)
            .frequent(DEFAULT_FREQUENT)
            .timeC(DEFAULT_TIME_C)
            .econoS(DEFAULT_ECONO_S);
        return decentralizeDecisionMaking;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DecentralizeDecisionMaking createUpdatedEntity() {
        DecentralizeDecisionMaking decentralizeDecisionMaking = new DecentralizeDecisionMaking()
            .group(UPDATED_GROUP)
            .classNum(UPDATED_CLASS_NUM)
            .decision(UPDATED_DECISION)
            .frequent(UPDATED_FREQUENT)
            .timeC(UPDATED_TIME_C)
            .econoS(UPDATED_ECONO_S);
        return decentralizeDecisionMaking;
    }

    @BeforeEach
    public void initTest() {
        decentralizeDecisionMakingRepository.deleteAll();
        decentralizeDecisionMaking = createEntity();
    }

    @Test
    public void createDecentralizeDecisionMaking() throws Exception {
        int databaseSizeBeforeCreate = decentralizeDecisionMakingRepository.findAll().size();
        // Create the DecentralizeDecisionMaking
        restDecentralizeDecisionMakingMockMvc.perform(post("/api/decentralize-decision-makings").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(decentralizeDecisionMaking)))
            .andExpect(status().isCreated());

        // Validate the DecentralizeDecisionMaking in the database
        List<DecentralizeDecisionMaking> decentralizeDecisionMakingList = decentralizeDecisionMakingRepository.findAll();
        assertThat(decentralizeDecisionMakingList).hasSize(databaseSizeBeforeCreate + 1);
        DecentralizeDecisionMaking testDecentralizeDecisionMaking = decentralizeDecisionMakingList.get(decentralizeDecisionMakingList.size() - 1);
        assertThat(testDecentralizeDecisionMaking.getGroup()).isEqualTo(DEFAULT_GROUP);
        assertThat(testDecentralizeDecisionMaking.getClassNum()).isEqualTo(DEFAULT_CLASS_NUM);
        assertThat(testDecentralizeDecisionMaking.getDecision()).isEqualTo(DEFAULT_DECISION);
        assertThat(testDecentralizeDecisionMaking.isFrequent()).isEqualTo(DEFAULT_FREQUENT);
        assertThat(testDecentralizeDecisionMaking.isTimeC()).isEqualTo(DEFAULT_TIME_C);
        assertThat(testDecentralizeDecisionMaking.isEconoS()).isEqualTo(DEFAULT_ECONO_S);
    }

    @Test
    public void createDecentralizeDecisionMakingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = decentralizeDecisionMakingRepository.findAll().size();

        // Create the DecentralizeDecisionMaking with an existing ID
        decentralizeDecisionMaking.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restDecentralizeDecisionMakingMockMvc.perform(post("/api/decentralize-decision-makings").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(decentralizeDecisionMaking)))
            .andExpect(status().isBadRequest());

        // Validate the DecentralizeDecisionMaking in the database
        List<DecentralizeDecisionMaking> decentralizeDecisionMakingList = decentralizeDecisionMakingRepository.findAll();
        assertThat(decentralizeDecisionMakingList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllDecentralizeDecisionMakings() throws Exception {
        // Initialize the database
        decentralizeDecisionMakingRepository.save(decentralizeDecisionMaking);

        // Get all the decentralizeDecisionMakingList
        restDecentralizeDecisionMakingMockMvc.perform(get("/api/decentralize-decision-makings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(decentralizeDecisionMaking.getId())))
            .andExpect(jsonPath("$.[*].group").value(hasItem(DEFAULT_GROUP)))
            .andExpect(jsonPath("$.[*].classNum").value(hasItem(DEFAULT_CLASS_NUM)))
            .andExpect(jsonPath("$.[*].decision").value(hasItem(DEFAULT_DECISION)))
            .andExpect(jsonPath("$.[*].frequent").value(hasItem(DEFAULT_FREQUENT.booleanValue())))
            .andExpect(jsonPath("$.[*].timeC").value(hasItem(DEFAULT_TIME_C.booleanValue())))
            .andExpect(jsonPath("$.[*].econoS").value(hasItem(DEFAULT_ECONO_S.booleanValue())));
    }
    
    @Test
    public void getDecentralizeDecisionMaking() throws Exception {
        // Initialize the database
        decentralizeDecisionMakingRepository.save(decentralizeDecisionMaking);

        // Get the decentralizeDecisionMaking
        restDecentralizeDecisionMakingMockMvc.perform(get("/api/decentralize-decision-makings/{id}", decentralizeDecisionMaking.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(decentralizeDecisionMaking.getId()))
            .andExpect(jsonPath("$.group").value(DEFAULT_GROUP))
            .andExpect(jsonPath("$.classNum").value(DEFAULT_CLASS_NUM))
            .andExpect(jsonPath("$.decision").value(DEFAULT_DECISION))
            .andExpect(jsonPath("$.frequent").value(DEFAULT_FREQUENT.booleanValue()))
            .andExpect(jsonPath("$.timeC").value(DEFAULT_TIME_C.booleanValue()))
            .andExpect(jsonPath("$.econoS").value(DEFAULT_ECONO_S.booleanValue()));
    }
    @Test
    public void getNonExistingDecentralizeDecisionMaking() throws Exception {
        // Get the decentralizeDecisionMaking
        restDecentralizeDecisionMakingMockMvc.perform(get("/api/decentralize-decision-makings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateDecentralizeDecisionMaking() throws Exception {
        // Initialize the database
        decentralizeDecisionMakingRepository.save(decentralizeDecisionMaking);

        int databaseSizeBeforeUpdate = decentralizeDecisionMakingRepository.findAll().size();

        // Update the decentralizeDecisionMaking
        DecentralizeDecisionMaking updatedDecentralizeDecisionMaking = decentralizeDecisionMakingRepository.findById(decentralizeDecisionMaking.getId()).get();
        updatedDecentralizeDecisionMaking
            .group(UPDATED_GROUP)
            .classNum(UPDATED_CLASS_NUM)
            .decision(UPDATED_DECISION)
            .frequent(UPDATED_FREQUENT)
            .timeC(UPDATED_TIME_C)
            .econoS(UPDATED_ECONO_S);

        restDecentralizeDecisionMakingMockMvc.perform(put("/api/decentralize-decision-makings").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedDecentralizeDecisionMaking)))
            .andExpect(status().isOk());

        // Validate the DecentralizeDecisionMaking in the database
        List<DecentralizeDecisionMaking> decentralizeDecisionMakingList = decentralizeDecisionMakingRepository.findAll();
        assertThat(decentralizeDecisionMakingList).hasSize(databaseSizeBeforeUpdate);
        DecentralizeDecisionMaking testDecentralizeDecisionMaking = decentralizeDecisionMakingList.get(decentralizeDecisionMakingList.size() - 1);
        assertThat(testDecentralizeDecisionMaking.getGroup()).isEqualTo(UPDATED_GROUP);
        assertThat(testDecentralizeDecisionMaking.getClassNum()).isEqualTo(UPDATED_CLASS_NUM);
        assertThat(testDecentralizeDecisionMaking.getDecision()).isEqualTo(UPDATED_DECISION);
        assertThat(testDecentralizeDecisionMaking.isFrequent()).isEqualTo(UPDATED_FREQUENT);
        assertThat(testDecentralizeDecisionMaking.isTimeC()).isEqualTo(UPDATED_TIME_C);
        assertThat(testDecentralizeDecisionMaking.isEconoS()).isEqualTo(UPDATED_ECONO_S);
    }

    @Test
    public void updateNonExistingDecentralizeDecisionMaking() throws Exception {
        int databaseSizeBeforeUpdate = decentralizeDecisionMakingRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDecentralizeDecisionMakingMockMvc.perform(put("/api/decentralize-decision-makings").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(decentralizeDecisionMaking)))
            .andExpect(status().isBadRequest());

        // Validate the DecentralizeDecisionMaking in the database
        List<DecentralizeDecisionMaking> decentralizeDecisionMakingList = decentralizeDecisionMakingRepository.findAll();
        assertThat(decentralizeDecisionMakingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteDecentralizeDecisionMaking() throws Exception {
        // Initialize the database
        decentralizeDecisionMakingRepository.save(decentralizeDecisionMaking);

        int databaseSizeBeforeDelete = decentralizeDecisionMakingRepository.findAll().size();

        // Delete the decentralizeDecisionMaking
        restDecentralizeDecisionMakingMockMvc.perform(delete("/api/decentralize-decision-makings/{id}", decentralizeDecisionMaking.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DecentralizeDecisionMaking> decentralizeDecisionMakingList = decentralizeDecisionMakingRepository.findAll();
        assertThat(decentralizeDecisionMakingList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
