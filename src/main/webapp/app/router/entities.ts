import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore

// prettier-ignore
const WeightedShortestJobFirst = () => import('@/entities/weighted-shortest-job-first/weighted-shortest-job-first.vue');
// prettier-ignore
const DecentralizeDecisionMaking = () => import('@/entities/decentralize-decision-making/decentralize-decision-making.vue');
// prettier-ignore

export default [
  {
    path: '/weighted-shortest-job-first/:weightedShortestJobFirstGroup',
    name: 'WeightedShortestJobFirst',
    component: WeightedShortestJobFirst,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/decentralize-decision-making',
    name: 'DecentralizeDecisionMaking',
    component: DecentralizeDecisionMaking,
    meta: { authorities: [Authority.USER] },
  },

];
