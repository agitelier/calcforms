export interface IDecentralizeDecisionMaking {
  id?: string;
  group?: string;
  classNum?: string;
  decision?: string;
  frequent?: boolean;
  timeC?: boolean;
  econoS?: boolean;
}

export class DecentralizeDecisionMaking implements IDecentralizeDecisionMaking {
  constructor(
    public id?: string,
    public group?: string,
    public classNum?: string,
    public decision?: string,
    public frequent?: boolean,
    public timeC?: boolean,
    public econoS?: boolean
  ) {
    this.frequent = this.frequent || false;
    this.timeC = this.timeC || false;
    this.econoS = this.econoS || false;
  }
}
