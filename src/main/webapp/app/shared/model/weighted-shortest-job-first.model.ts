export interface IWeightedShortestJobFirst {
  id?: string;
  group?: string;
  classNum?: string;
  feature?: string;
  userBV?: number;
  timeC?: number;
  rroe?: number;
  jobS?: number;
}

export class WeightedShortestJobFirst implements IWeightedShortestJobFirst {
  constructor(
    public id?: string,
    public group?: string,
    public classNum?: string,
    public feature?: string,
    public userBV?: number,
    public timeC?: number,
    public rroe?: number,
    public jobS?: number
  ) {}
}
