import axios from 'axios';

import { IDecentralizeDecisionMaking, DecentralizeDecisionMaking } from '@/shared/model/decentralize-decision-making.model';

const baseApiUrl = 'api/decentralize-decision-makings';

export default class DecentralizeDecisionMakingService {
  /**
   * Fetch one entry in the database
   *
   * @param id the ID of the ddm to fetch
   * @returns the ddm with the corresponding ID
   */
  public find(id: string): Promise<IDecentralizeDecisionMaking> {
    return new Promise<IDecentralizeDecisionMaking>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  /**
   * Fetch all ddm in the database
   *
   * @deprecated use retrieveClassGroup instead
   * @returns All ddm in the database
   */
  public retrieve(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(baseApiUrl)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  /**
   * Delete one entry from the database
   *
   * @param id The id of the entry to be deleted
   * @returns No content
   */
  public delete(id: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .delete(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  /**
   * Create a ddm in the database
   *
   * @param entity a ddm to create in the database
   * @returns The created ddm
   */
  public create(entity: IDecentralizeDecisionMaking): Promise<IDecentralizeDecisionMaking> {
    return new Promise<IDecentralizeDecisionMaking>((resolve, reject) => {
      axios
        .post(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  /**
   * Create an empty ddm in the database
   *
   * @param classNum the class of the new entry
   * @param group the sub group of the new entry
   * @returns The created ddm
   */
  public createNew(classNum, group): Promise<IDecentralizeDecisionMaking> {
    const theEntity: IDecentralizeDecisionMaking = new DecentralizeDecisionMaking();
    theEntity.classNum = classNum;
    theEntity.group = group;
    return this.create(theEntity);
  }

  /**
   * Update a ddm in the database
   *
   * @param entity the ddm you want to update in the database
   * @returns the updated ddm
   */
  public update(entity: IDecentralizeDecisionMaking): Promise<IDecentralizeDecisionMaking> {
    return new Promise<IDecentralizeDecisionMaking>((resolve, reject) => {
      axios
        .put(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  /**
   * Fetch all ddm in the database with the given classNum and group
   *
   * @param classNum the group you want to fetch the entry from
   * @param group the sub group you want to fetch the entry from
   * @returns All ddm in the database with the right class and group
   */
  public retrieveClassGroup(classNum, group): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/${classNum}/${group}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}
