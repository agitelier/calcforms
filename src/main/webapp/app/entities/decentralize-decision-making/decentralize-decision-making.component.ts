import { mixins } from 'vue-class-component';

import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IDecentralizeDecisionMaking } from '@/shared/model/decentralize-decision-making.model';
import AlertMixin from '@/shared/alert/alert.mixin';

import DecentralizeDecisionMakingService from './decentralize-decision-making.service';
import { integer, not } from 'vuelidate/lib/validators';
import HtmlWebpackPlugin from 'html-webpack-plugin';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class DecentralizeDecisionMaking extends mixins(AlertMixin) {
  @Inject('decentralizeDecisionMakingService') private decentralizeDecisionMakingService: () => DecentralizeDecisionMakingService;
  private removeId: string = null;
  public decentralizeDecisionMakings: IDecentralizeDecisionMaking[] = [];
  public isFetching = false;
  private isSaving = false;

  /**
   * Auto-generated method. Launched first when the page is loaded.
   */
  public mounted(): void {
    this.retrieveAllDecentralizeDecisionMakings();
  }

  /**
   * Auto-generated method.
   */
  public clear(): void {
    this.retrieveAllDecentralizeDecisionMakings();
  }

  /** Update the value of this.decentralizeDecisionMakings
   *
   * By fetching all data with the class and group in the route/url
   */
  public retrieveAllDecentralizeDecisionMakings(): void {
    this.isFetching = true;

    this.decentralizeDecisionMakingService()
      .retrieveClassGroup('defaultClass', this.$route.params.decentralizeDecisionMakingGroup)
      .then(
        res => {
          this.decentralizeDecisionMakings = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  /**
   * Store the id of the instance and then display the pop-up menu confirming the deletion
   *
   * @param instance an ddm entry stored in the database that need to be removed
   */
  public prepareRemove(instance: IDecentralizeDecisionMaking): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  /**
   * Delete the ddm which have been passed last to prepareRemove.
   */
  public removeDecentralizeDecisionMaking(): void {
    this.decentralizeDecisionMakingService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('calcformsApp.decentralizeDecisionMaking.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();
        this.removeId = null;
        this.retrieveAllDecentralizeDecisionMakings();
        this.closeDialog();
      });
  }

  /**
   * Close the pop-up.
   */
  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }

  /**
   * Save the modification made to the instance.
   *
   * @param instance an dmm entry stored in the database that need to be updated.
   */
  public save(instance: IDecentralizeDecisionMaking): void {
    if (this.validerForm(event) === true) {
      this.decentralizeDecisionMakingService().update(instance);
    } else {
      this.save(instance);
    }
  }

  public addRow(): void {
    this.isSaving = true;
    this.decentralizeDecisionMakingService()
      .createNew('defaultClass', this.$route.params.decentralizeDecisionMakingGroup)
      .then(param => {
        this.isSaving = false;
        const message = this.$t('calcformsApp.decentralizeDecisionMaking.created', { param: param.id });
        this.alertService().showAlert(message, 'success');
        this.getAlertFromStore();
        this.retrieveAllDecentralizeDecisionMakings();
      });
  }

  /**
   * Calcul the value of the Total field in the ddm table
   *
   * @param frequent boolean from the table
   * @param timeC boolean from the table
   * @param econoS boolean from the table
   * @returns the value of the Total field
   */
  public calculTotal(frequent: boolean, timeC: boolean, econoS: boolean) {
    let total = 0;
    if (frequent === true) {
      total = total + 2;
    } else {
      total = total + 0;
    }
    if (timeC === true) {
      total = total + 2;
    } else {
      total = total + 0;
    }
    if (econoS === true) {
      total = total + 0;
    } else {
      total = total + 2;
    }
    return total;
  }

  /**
   * Validate the field in the form
   *
   * @todo doesn't look like it work as expected
   */
  public validerForm(e) {
    e.preventDefault();

    const table = (<HTMLTableElement>document.getElementById('myTable')).rows.length;

    for (let i = 0, j = 0; i < (table - 1) * 4; i += 4, j++) {
      const elem = document.getElementsByTagName('input')[i].value;
      const erreurDecision = document.getElementsByTagName('p')[j];
      if (elem === '') {
        erreurDecision.textContent = 'Enter the decision ';
        return false;
      }
      erreurDecision.textContent = '';
    }
    return true;
  }
}
