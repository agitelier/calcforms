import axios from 'axios';

import { IWeightedShortestJobFirst, WeightedShortestJobFirst } from '@/shared/model/weighted-shortest-job-first.model';

const baseApiUrl = 'api/weighted-shortest-job-firsts';

export default class WeightedShortestJobFirstService {
  /**
   * Fetch one entry in the database
   *
   * @param id the ID of the wsjf to fetch
   * @returns the wsjf with the corresponding ID
   */
  public find(id: string): Promise<IWeightedShortestJobFirst> {
    return new Promise<IWeightedShortestJobFirst>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  /**
   * Fetch all wsjf in the database
   *
   * @deprecated use retrieveClassGroup instead
   * @returns All wsjf in the database
   */
  public retrieve(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(baseApiUrl)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  /**
   * Delete one entry from the database
   *
   * @param id The id of the entry to be deleted
   * @returns No content
   */
  public delete(id: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .delete(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  /**
   * Create a wsjf in the database
   *
   * @param entity a wsjf to create in the database
   * @returns The created wsjf
   */
  public create(entity: IWeightedShortestJobFirst): Promise<IWeightedShortestJobFirst> {
    return new Promise<IWeightedShortestJobFirst>((resolve, reject) => {
      axios
        .post(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  /**
   * Create an empty wsjf in the database
   *
   * @param classNum the class of the new entry
   * @param group the sub group of the new entry
   * @returns The created wsjf
   */
  public createNew(classNum, group): Promise<IWeightedShortestJobFirst> {
    const theEntity: IWeightedShortestJobFirst = new WeightedShortestJobFirst();
    theEntity.classNum = classNum;
    theEntity.group = group;
    return this.create(theEntity);
  }

  /**
   * Update a wsjf in the database
   *
   * @param entity the wsjf you want to update in the database
   * @returns the updated wsjf
   */
  public update(entity: IWeightedShortestJobFirst): Promise<IWeightedShortestJobFirst> {
    return new Promise<IWeightedShortestJobFirst>((resolve, reject) => {
      axios
        .put(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  /**
   * Fetch all wsjf in the database with the given classNum and group
   *
   * @param classNum the group you want to fetch the entry from
   * @param group the sub group you want to fetch the entry from
   * @returns All wsjf in the database with the right class and group
   */
  public retrieveClassGroup(classNum, group): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/${classNum}/${group}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}
