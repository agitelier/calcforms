import * as SockJS from 'sockjs-client';
import * as Stomp from 'webstomp-client';
import { Observable, Observer } from 'rxjs';
import VueRouter from 'vue-router';

import { IWeightedShortestJobFirst } from '@/shared/model/weighted-shortest-job-first.model';

export default class TrackerService {
  public stompClient: Stomp.Client = null;
  public subscriber: any = null;
  public connection: Promise<any>;
  public connectedPromise: any = null;
  public listener: Observable<any>;
  public listenerObserver: Observer<any> = null;

  private router: VueRouter;

  constructor(router: VueRouter) {
    this.router = router;
    this.connection = this.createConnection();
    this.listener = this.createListener();
  }

  public connect(): void {
    if (this.connectedPromise === null) {
      this.connection = this.createConnection();
    }
    // building absolute path so that websocket doesn't fail when deploying with a context path
    const loc = window.location;
    const baseHref = document.querySelector('base').getAttribute('href');
    let url;
    url = '//' + loc.host + baseHref + 'websocket/wsjfTracker';
    const socket = new SockJS(url);
    this.stompClient = Stomp.over(socket);
    const headers = {};
    this.stompClient.connect(headers, () => this.afterConnect());
  }

  public afterConnect() {
    this.connectedPromise('success');
    this.connectedPromise = null;
  }

  public disconnect(): void {
    if (this.stompClient !== null) {
      this.stompClient.disconnect();
      this.stompClient = null;
    }
    this.router.afterEach(() => {});
  }

  public receive(): Observable<any> {
    return this.listener;
  }

  public sendActivity(instance: IWeightedShortestJobFirst, myAction): void {
    if (this.stompClient !== null && this.stompClient.connected) {
      this.stompClient.send(
        '/topic/myActivity', // destination
        JSON.stringify({
          action: myAction,
          id: instance.id,
          group: instance.group,
          classNum: instance.classNum,
          feature: instance.feature,
          userBV: instance.userBV,
          timeC: instance.timeC,
          rroe: instance.rroe,
          jobS: instance.jobS,
        }), // body
        {} // header
      );
    }
  }

  public subscribe(): void {
    this.connection.then(() => {
      this.subscriber = this.stompClient.subscribe('/topic/wsjfTracker', data => {
        this.listenerObserver.next(JSON.parse(data.body));
      });
    });
  }

  public unsubscribe(): any {
    if (this.subscriber !== null) {
      this.subscriber.unsubscribe();
    }
    this.listener = this.createListener();
  }

  public createListener(): any {
    return new Observable(observer => {
      this.listenerObserver = observer;
    });
  }

  public createConnection(): Promise<any> {
    return new Promise((resolve, reject) => (this.connectedPromise = resolve));
  }
}
