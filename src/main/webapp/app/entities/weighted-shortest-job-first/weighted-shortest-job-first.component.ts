import { mixins } from 'vue-class-component';

import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IWeightedShortestJobFirst } from '@/shared/model/weighted-shortest-job-first.model';
import AlertMixin from '@/shared/alert/alert.mixin';

import WeightedShortestJobFirstService from './weighted-shortest-job-first.service';

import TrackerService from './tracker.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class WeightedShortestJobFirst extends mixins(AlertMixin) {
  private trackerService = null;
  @Inject('weightedShortestJobFirstService') private weightedShortestJobFirstService: () => WeightedShortestJobFirstService;
  private removeInstance = null;
  private removeId = false;
  private isSaving = false;

  MY_ADD = 1;
  MY_DELETE = 2;
  MY_MODIFY = 3;

  public weightedShortestJobFirsts: IWeightedShortestJobFirst[] = [];
  public isFetching = false;

  /**
   * Auto-generated method. Launched first when the page is loaded.
   */
  public mounted(): void {
    this.init();
  }

  public init(): void {
    this.trackerService = new TrackerService(this.$router);
    this.trackerService.connect();
    this.trackerService.subscribe();
    this.trackerService.receive().subscribe(myActivity => {
      this.updateWsjf(myActivity);
    });
    this.retrieveAllWeightedShortestJobFirsts();
  }

  public async updateWsjf(myActivity: any) {
    if (
      myActivity.group == this.$route.params.weightedShortestJobFirstGroup &&
      myActivity.classNum == this.$route.params.weightedShortestJobFirstClassNum
    ) {
      let entity = await this.getEntity(myActivity);
      let myId = await this.getPlaceById(myActivity.id);
      if (myActivity.action == this.MY_ADD && myId == null) {
        this.weightedShortestJobFirsts.push(entity);
      } else if (myActivity.action == this.MY_MODIFY && myId != null) {
        this.weightedShortestJobFirsts.splice(myId, 1);
        this.weightedShortestJobFirsts.push(entity);
      } else if (myActivity.action == this.MY_DELETE && myId != null) {
        this.weightedShortestJobFirsts.splice(myId, 1);
      }
    }
  }

  public getEntity(myActivity): IWeightedShortestJobFirst {
    let entity: IWeightedShortestJobFirst = new WeightedShortestJobFirst();
    entity.id = myActivity.id;
    entity.group = myActivity.group;
    entity.classNum = myActivity.classNum;
    entity.feature = myActivity.feature;
    entity.userBV = myActivity.userBV;
    entity.timeC = myActivity.timeC;
    entity.rroe = myActivity.rroe;
    entity.jobS = myActivity.jobS;
    return entity;
  }

  public getPlaceById(id) {
    let exitIt = false;
    let place = null;
    let i;
    for (i = 0; i < this.weightedShortestJobFirsts.length && exitIt == false; i++) {
      if (this.weightedShortestJobFirsts[i].id == id) {
        place = i;
        exitIt = true;
      }
    }
    return place;
  }

  public clear(): void {
    this.retrieveAllWeightedShortestJobFirsts();
  }

  /** Update the value of this.weightedShortestJobFirsts
   *
   * By fetching all data with the class and group in the route/url
   */
  public retrieveAllWeightedShortestJobFirsts(): void {
    this.isFetching = true;

    this.weightedShortestJobFirstService()
      .retrieveClassGroup('defaultClass', this.$route.params.weightedShortestJobFirstGroup)
      .then(
        res => {
          this.weightedShortestJobFirsts = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  /**
   * Store the id of the instance and then display the pop-up menu confirming the deletion
   *
   * @param instance an wsjf entry stored in the database that need to be removed
   */
  public prepareRemove(instance: IWeightedShortestJobFirst): void {
    this.removeInstance = instance;
    this.removeId = true;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  /**
   * Save the modification made to the instance.
   *
   * @param instance an wsjf entry stored in the database that need to be updated.
   */
  public save(instance: IWeightedShortestJobFirst): void {
    this.trackerService.sendActivity(instance, this.MY_MODIFY);
    this.weightedShortestJobFirstService().update(instance);
    console.log('spam?');
  }

  public removeWeightedShortestJobFirst(): void {
    if (this.removeId == true) {
      this.trackerService.sendActivity(this.removeInstance, this.MY_DELETE);
      this.weightedShortestJobFirstService()
        .delete(this.removeInstance.id)
        .then(() => {
          const message = this.$t('calcformsApp.weightedShortestJobFirst.deleted', { param: this.removeInstance.id });
          this.alertService().showAlert(message, 'danger');
          this.getAlertFromStore();
          this.retrieveAllWeightedShortestJobFirsts();
          this.closeDialog();
          this.removeId = false;
        });
    }
  }

  /**
   * Close the pop-up.
   */
  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }

  /**
   * Add a new row to the mongo database for the class and group specified in the route/url.
   */
  public addRow(): void {
    this.isSaving = true;
    this.weightedShortestJobFirstService()
      .createNew('defaultClass', this.$route.params.weightedShortestJobFirstGroup)
      .then(param => {
        this.isSaving = false;
        const message = this.$t('calcformsApp.weightedShortestJobFirst.created', { param: param.id });
        this.alertService().showAlert(message, 'success');
        this.getAlertFromStore();
        this.retrieveAllWeightedShortestJobFirsts();
        this.trackerService.sendActivity(param, this.MY_ADD);
      });
  }
}
