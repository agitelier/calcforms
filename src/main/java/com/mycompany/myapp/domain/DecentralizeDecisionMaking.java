package com.mycompany.myapp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

/**
 * A DecentralizeDecisionMaking.
 */
@Document(collection = "decentralize_decision_making")
public class DecentralizeDecisionMaking implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("group")
    private String group;

    @Field("class_num")
    private String classNum;

    @Field("decision")
    private String decision;

    @Field("frequent")
    private Boolean frequent;

    @Field("time_c")
    private Boolean timeC;

    @Field("econo_s")
    private Boolean econoS;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public DecentralizeDecisionMaking group(String group) {
        this.group = group;
        return this;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getClassNum() {
        return classNum;
    }

    public DecentralizeDecisionMaking classNum(String classNum) {
        this.classNum = classNum;
        return this;
    }

    public void setClassNum(String classNum) {
        this.classNum = classNum;
    }

    public String getDecision() {
        return decision;
    }

    public DecentralizeDecisionMaking decision(String decision) {
        this.decision = decision;
        return this;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    public Boolean isFrequent() {
        return frequent;
    }

    public DecentralizeDecisionMaking frequent(Boolean frequent) {
        this.frequent = frequent;
        return this;
    }

    public void setFrequent(Boolean frequent) {
        this.frequent = frequent;
    }

    public Boolean isTimeC() {
        return timeC;
    }

    public DecentralizeDecisionMaking timeC(Boolean timeC) {
        this.timeC = timeC;
        return this;
    }

    public void setTimeC(Boolean timeC) {
        this.timeC = timeC;
    }

    public Boolean isEconoS() {
        return econoS;
    }

    public DecentralizeDecisionMaking econoS(Boolean econoS) {
        this.econoS = econoS;
        return this;
    }

    public void setEconoS(Boolean econoS) {
        this.econoS = econoS;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DecentralizeDecisionMaking)) {
            return false;
        }
        return id != null && id.equals(((DecentralizeDecisionMaking) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DecentralizeDecisionMaking{" +
            "id=" + getId() +
            ", group='" + getGroup() + "'" +
            ", classNum='" + getClassNum() + "'" +
            ", decision='" + getDecision() + "'" +
            ", frequent='" + isFrequent() + "'" +
            ", timeC='" + isTimeC() + "'" +
            ", econoS='" + isEconoS() + "'" +
            "}";
    }
}
