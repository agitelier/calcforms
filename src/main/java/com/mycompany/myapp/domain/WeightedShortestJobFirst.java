package com.mycompany.myapp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

/**
 * A WeightedShortestJobFirst.
 */
@Document(collection = "weighted_shortest_job_first")
public class WeightedShortestJobFirst implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("group")
    private String group;

    @Field("class_num")
    private String classNum;

    @Field("feature")
    private String feature;

    @Field("user_bv")
    private Integer userBV;

    @Field("time_c")
    private Integer timeC;

    @Field("rroe")
    private Integer rroe;

    @Field("job_s")
    private Integer jobS;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public WeightedShortestJobFirst group(String group) {
        this.group = group;
        return this;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getClassNum() {
        return classNum;
    }

    public WeightedShortestJobFirst classNum(String classNum) {
        this.classNum = classNum;
        return this;
    }

    public void setClassNum(String classNum) {
        this.classNum = classNum;
    }

    public String getFeature() {
        return feature;
    }

    public WeightedShortestJobFirst feature(String feature) {
        this.feature = feature;
        return this;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public Integer getUserBV() {
        return userBV;
    }

    public WeightedShortestJobFirst userBV(Integer userBV) {
        this.userBV = userBV;
        return this;
    }

    public void setUserBV(Integer userBV) {
        this.userBV = userBV;
    }

    public Integer getTimeC() {
        return timeC;
    }

    public WeightedShortestJobFirst timeC(Integer timeC) {
        this.timeC = timeC;
        return this;
    }

    public void setTimeC(Integer timeC) {
        this.timeC = timeC;
    }

    public Integer getRroe() {
        return rroe;
    }

    public WeightedShortestJobFirst rroe(Integer rroe) {
        this.rroe = rroe;
        return this;
    }

    public void setRroe(Integer rroe) {
        this.rroe = rroe;
    }

    public Integer getJobS() {
        return jobS;
    }

    public WeightedShortestJobFirst jobS(Integer jobS) {
        this.jobS = jobS;
        return this;
    }

    public void setJobS(Integer jobS) {
        this.jobS = jobS;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof WeightedShortestJobFirst)) {
            return false;
        }
        return id != null && id.equals(((WeightedShortestJobFirst) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "WeightedShortestJobFirst{" +
            "id=" + getId() +
            ", group='" + getGroup() + "'" +
            ", classNum='" + getClassNum() + "'" +
            ", feature='" + getFeature() + "'" +
            ", userBV=" + getUserBV() +
            ", timeC=" + getTimeC() +
            ", rroe=" + getRroe() +
            ", jobS=" + getJobS() +
            "}";
    }
}
