package com.mycompany.myapp.web.api;

import com.mycompany.myapp.web.api.model.Variant;
import com.mycompany.myapp.web.api.VariantesApiDelegate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class MyVariantesApi implements VariantesApiDelegate{

    @Override
    public ResponseEntity<Variant> getWorkshopVariant(String id) { 
        if (id.equals("weighted-shortest-job-first") || id.equals("decentralize-decision-making")){
            return new ResponseEntity<>(getVariant(id), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<List<Variant>> getWorkshopVariants() {
        ArrayList<Variant> list = new ArrayList<Variant>();
        list.add(getVariant("weighted-shortest-job-first"));
        list.add(getVariant("decentralize-decision-making"));
        return new ResponseEntity<>(list, HttpStatus.OK);

    }

    private Variant getVariant(String variantId){
        Variant variant = new Variant();
        if (variantId.equals("weighted-shortest-job-first")){
            variant.setId("weighted-shortest-job-first");
            variant.setTitle("Weighted Shortest Job First");
            variant.setDescription("A realtime collaborative tool to make decision based on the importance of a task and it's lenght.");
        } else if (variantId.equals("decentralize-decision-making")){
        Variant ddm = new Variant();
            variant.setId("decentralize-decision-making");
            variant.setTitle("Decentralised Decision Making");
            variant.setDescription("A Yes/No table to help decide if a decision should be decentralised");
        }
        return variant;
    }

}
