package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.DecentralizeDecisionMaking;
import com.mycompany.myapp.repository.DecentralizeDecisionMakingRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import org.springframework.data.domain.Example;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.DecentralizeDecisionMaking}.
 */
@RestController
@RequestMapping("/api")
public class DecentralizeDecisionMakingResource {

    private final Logger log = LoggerFactory.getLogger(DecentralizeDecisionMakingResource.class);

    private static final String ENTITY_NAME = "decentralizeDecisionMaking";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DecentralizeDecisionMakingRepository decentralizeDecisionMakingRepository;

    public DecentralizeDecisionMakingResource(DecentralizeDecisionMakingRepository decentralizeDecisionMakingRepository) {
        this.decentralizeDecisionMakingRepository = decentralizeDecisionMakingRepository;
    }

    /**
     * {@code POST  /decentralize-decision-makings} : Create a new decentralizeDecisionMaking.
     *
     * @param decentralizeDecisionMaking the decentralizeDecisionMaking to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new decentralizeDecisionMaking, or with status {@code 400 (Bad Request)} if the decentralizeDecisionMaking has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/decentralize-decision-makings")
    public ResponseEntity<DecentralizeDecisionMaking> createDecentralizeDecisionMaking(@RequestBody DecentralizeDecisionMaking decentralizeDecisionMaking) throws URISyntaxException {
        log.debug("REST request to save DecentralizeDecisionMaking : {}", decentralizeDecisionMaking);
        if (decentralizeDecisionMaking.getId() != null) {
            throw new BadRequestAlertException("A new decentralizeDecisionMaking cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DecentralizeDecisionMaking result = decentralizeDecisionMakingRepository.save(decentralizeDecisionMaking);
        return ResponseEntity.created(new URI("/api/decentralize-decision-makings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /decentralize-decision-makings} : Updates an existing decentralizeDecisionMaking.
     *
     * @param decentralizeDecisionMaking the decentralizeDecisionMaking to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated decentralizeDecisionMaking,
     * or with status {@code 400 (Bad Request)} if the decentralizeDecisionMaking is not valid,
     * or with status {@code 500 (Internal Server Error)} if the decentralizeDecisionMaking couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/decentralize-decision-makings")
    public ResponseEntity<DecentralizeDecisionMaking> updateDecentralizeDecisionMaking(@RequestBody DecentralizeDecisionMaking decentralizeDecisionMaking) throws URISyntaxException {
        log.debug("REST request to update DecentralizeDecisionMaking : {}", decentralizeDecisionMaking);
        if (decentralizeDecisionMaking.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DecentralizeDecisionMaking result = decentralizeDecisionMakingRepository.save(decentralizeDecisionMaking);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, decentralizeDecisionMaking.getId()))
            .body(result);
    }

    /**
     * {@code GET  /decentralize-decision-makings} : get all the decentralizeDecisionMakings.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of decentralizeDecisionMakings in body.
     */
    @GetMapping("/decentralize-decision-makings")
    public List<DecentralizeDecisionMaking> getAllDecentralizeDecisionMakings() {
        log.debug("REST request to get all DecentralizeDecisionMakings");
        return decentralizeDecisionMakingRepository.findAll();
    }

    /**
     * {@code GET  /decentralize-decision-makings/:id} : get the "id" decentralizeDecisionMaking.
     *
     * @param id the id of the decentralizeDecisionMaking to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the decentralizeDecisionMaking, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/decentralize-decision-makings/{id}")
    public ResponseEntity<DecentralizeDecisionMaking> getDecentralizeDecisionMaking(@PathVariable String id) {
        log.debug("REST request to get DecentralizeDecisionMaking : {}", id);
        Optional<DecentralizeDecisionMaking> decentralizeDecisionMaking = decentralizeDecisionMakingRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(decentralizeDecisionMaking);
    }

    /**
     * {@code DELETE  /decentralize-decision-makings/:id} : delete the "id" decentralizeDecisionMaking.
     *
     * @param id the id of the decentralizeDecisionMaking to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/decentralize-decision-makings/{id}")
    public ResponseEntity<Void> deleteDecentralizeDecisionMaking(@PathVariable String id) {
        log.debug("REST request to delete DecentralizeDecisionMaking : {}", id);
        decentralizeDecisionMakingRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }

    /**
     * {@code GET  /decentralize-decision-makings/:classNum/:group} : get all the decentralizeDecisionMakings for a group and class.
     *
     * @param classNum the class of the ddm to retrieve.
     * @param group the sub-group of the ddm to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of decentralizeDecisionMakings in body.
     */
        @GetMapping("/decentralize-decision-makings/{classNum}/{group}")
    public List<DecentralizeDecisionMaking> getDecentralizeDecisionMakingByClassAndGroup(@PathVariable String classNum, @PathVariable String group) {
        log.debug("REST request to get all WeightedShortestJobFirsts of the class and group");
        DecentralizeDecisionMaking ddm = new DecentralizeDecisionMaking();
        ddm.setClassNum(classNum);
        ddm.setGroup(group);
        Example<DecentralizeDecisionMaking> myMatcher = Example.of(ddm);
        return decentralizeDecisionMakingRepository.findAll(myMatcher);
    }

}
