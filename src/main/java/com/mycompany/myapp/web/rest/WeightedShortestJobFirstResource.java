package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.WeightedShortestJobFirst;
import com.mycompany.myapp.repository.WeightedShortestJobFirstRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import org.springframework.data.domain.Example;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.WeightedShortestJobFirst}.
 */
@RestController
@RequestMapping("/api")
public class WeightedShortestJobFirstResource {

    private final Logger log = LoggerFactory.getLogger(WeightedShortestJobFirstResource.class);

    private static final String ENTITY_NAME = "weightedShortestJobFirst";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WeightedShortestJobFirstRepository weightedShortestJobFirstRepository;

    public WeightedShortestJobFirstResource(WeightedShortestJobFirstRepository weightedShortestJobFirstRepository) {
        this.weightedShortestJobFirstRepository = weightedShortestJobFirstRepository;
    }

    /**
     * {@code POST  /weighted-shortest-job-firsts} : Create a new weightedShortestJobFirst.
     *
     * @param weightedShortestJobFirst the weightedShortestJobFirst to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new weightedShortestJobFirst, or with status {@code 400 (Bad Request)} if the weightedShortestJobFirst has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/weighted-shortest-job-firsts")
    public ResponseEntity<WeightedShortestJobFirst> createWeightedShortestJobFirst(@RequestBody WeightedShortestJobFirst weightedShortestJobFirst) throws URISyntaxException {
        log.debug("REST request to save WeightedShortestJobFirst : {}", weightedShortestJobFirst);
        if (weightedShortestJobFirst.getId() != null) {
            throw new BadRequestAlertException("A new weightedShortestJobFirst cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WeightedShortestJobFirst result = weightedShortestJobFirstRepository.save(weightedShortestJobFirst);
        return ResponseEntity.created(new URI("/api/weighted-shortest-job-firsts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /weighted-shortest-job-firsts} : Updates an existing weightedShortestJobFirst.
     *
     * @param weightedShortestJobFirst the weightedShortestJobFirst to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated weightedShortestJobFirst,
     * or with status {@code 400 (Bad Request)} if the weightedShortestJobFirst is not valid,
     * or with status {@code 500 (Internal Server Error)} if the weightedShortestJobFirst couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/weighted-shortest-job-firsts")
    public ResponseEntity<WeightedShortestJobFirst> updateWeightedShortestJobFirst(@RequestBody WeightedShortestJobFirst weightedShortestJobFirst) throws URISyntaxException {
        log.debug("REST request to update WeightedShortestJobFirst : {}", weightedShortestJobFirst);
        if (weightedShortestJobFirst.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WeightedShortestJobFirst result = weightedShortestJobFirstRepository.save(weightedShortestJobFirst);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, weightedShortestJobFirst.getId()))
            .body(result);
    }

    /**
     * {@code GET  /weighted-shortest-job-firsts} : get all the weightedShortestJobFirsts.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of weightedShortestJobFirsts in body.
     */
    @GetMapping("/weighted-shortest-job-firsts")
    public List<WeightedShortestJobFirst> getAllWeightedShortestJobFirsts() {
        log.debug("REST request to get all WeightedShortestJobFirsts");
        return weightedShortestJobFirstRepository.findAll();
    }

    /**
     * {@code GET  /weighted-shortest-job-firsts/:id} : get the "id" weightedShortestJobFirst.
     *
     * @param id the id of the weightedShortestJobFirst to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the weightedShortestJobFirst, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/weighted-shortest-job-firsts/{id}")
    public ResponseEntity<WeightedShortestJobFirst> getWeightedShortestJobFirst(@PathVariable String id) {
        log.debug("REST request to get WeightedShortestJobFirst : {}", id);
        Optional<WeightedShortestJobFirst> weightedShortestJobFirst = weightedShortestJobFirstRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(weightedShortestJobFirst);
    }

    /**
     * {@code DELETE  /weighted-shortest-job-firsts/:id} : delete the "id" weightedShortestJobFirst.
     *
     * @param id the id of the weightedShortestJobFirst to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/weighted-shortest-job-firsts/{id}")
    public ResponseEntity<Void> deleteWeightedShortestJobFirst(@PathVariable String id) {
        log.debug("REST request to delete WeightedShortestJobFirst : {}", id);
        weightedShortestJobFirstRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
 
     /**
     * {@code GET  /weighted-shortest-job-firsts/:classNum/:group} : get all the weightedShortestJobFirsts of this class and group.
     *
     * @param classNum the class of the weightedShortestJobFirsts to retrieve.
     * @param group the group of the weightedShortestJobFirsts to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of weightedShortestJobFirsts in body.
     */
    @GetMapping("/weighted-shortest-job-firsts/{classNum}/{group}")
    public List<WeightedShortestJobFirst> getWeightedShortestJobFirstsByClassAndGroup(@PathVariable String classNum, @PathVariable String group) {
        log.debug("REST request to get all WeightedShortestJobFirsts of the class and group");
        WeightedShortestJobFirst wsjf = new WeightedShortestJobFirst();
        wsjf.setClassNum(classNum);
        wsjf.setGroup(group);
        Example<WeightedShortestJobFirst> myMatcher = Example.of(wsjf);
        return weightedShortestJobFirstRepository.findAll(myMatcher);
    }
}
