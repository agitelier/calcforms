package com.mycompany.myapp.web.websocket.dto;

import java.time.Instant;

/**
 * DTO for storing a user's activity.
 */
public class MyActivityDTO {

	private String sessionId;

	private String userLogin;

	private String ipAddress;

	private Instant time;

    private Integer action;

	private String id;

	private String group;

	private String classNum;

	private String feature;

	private Integer userBV;

	private Integer timeC;

	private Integer rroe;

	private Integer jobS;

	public String getSessionId() {
		return this.sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUserLogin() {
		return this.userLogin;
	}

	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Instant getTime() {
		return this.time;
	}

	public void setTime(Instant time) {
		this.time = time;
	}

        public Integer getAction(){
                return this.action;
        }

        public void setAction(Integer action){
                this.action = action;
        }

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGroup() {
		return this.group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getClassNum() {
		return this.classNum;
	}

	public void setClassNum(String classNum) {
		this.classNum = classNum;
	}

	public String getFeature() {
		return this.feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	public Integer getUserBV() {
		return this.userBV;
	}

	public void setUserBV(Integer userBV) {
		this.userBV = userBV;
	}

	public Integer getTimeC() {
		return this.timeC;
	}

	public void setTimeC(Integer timeC) {
		this.timeC = timeC;
	}

	public Integer getRroe() {
		return this.rroe;
	}

	public void setRroe(Integer rroe) {
		this.rroe = rroe;
	}

	public Integer getJobS() {
		return this.jobS;
	}

	public void setJobS(Integer jobS) {
		this.jobS = jobS;
	}

	// prettier-ignore
	@Override
	public String toString() {
		return "MyActivityDTO{" + "sessionId='" + sessionId + '\'' + ", userLogin='" + userLogin + '\''
				+ ", ipAddress='" + ipAddress + '\'' + ", time='" + time + '\'' + ", action='"+ action + '\'' + ", id='" + id + '\'' + ", group='"
				+ group + '\'' + ", classNum='" + classNum + '\'' + ", feature='" + feature + '\'' + ", userBV='"
				+ userBV + '\'' + ", timeC='" + timeC + '\'' + ", rroe='" + rroe + '\'' + ", jobS='" + jobS + '\''
				+ '}';
	}
}
