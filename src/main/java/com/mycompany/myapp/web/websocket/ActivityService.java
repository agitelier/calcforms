package com.mycompany.myapp.web.websocket;

import static com.mycompany.myapp.config.WebsocketConfiguration.IP_ADDRESS;

import com.mycompany.myapp.web.websocket.dto.MyActivityDTO;

import java.security.Principal;
import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.handler.annotation.*;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

@Controller
public class ActivityService implements ApplicationListener<SessionDisconnectEvent> {

    private static final Logger log = LoggerFactory.getLogger(ActivityService.class);

    private final SimpMessageSendingOperations messagingTemplate;

    public ActivityService(SimpMessageSendingOperations messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @MessageMapping("/topic/myActivity")
    @SendTo("/topic/wsjfTracker")
    public MyActivityDTO sendActivity(@Payload MyActivityDTO myActivityDTO, StompHeaderAccessor stompHeaderAccessor, Principal principal) {
        myActivityDTO.setUserLogin(principal.getName());
        myActivityDTO.setSessionId(stompHeaderAccessor.getSessionId());
        myActivityDTO.setIpAddress(stompHeaderAccessor.getSessionAttributes().get(IP_ADDRESS).toString());
        myActivityDTO.setTime(Instant.now());
        log.debug("Sending user tracking data {}", myActivityDTO);
        return myActivityDTO;
    }

    @Override
    public void onApplicationEvent(SessionDisconnectEvent event) {
    }
}
