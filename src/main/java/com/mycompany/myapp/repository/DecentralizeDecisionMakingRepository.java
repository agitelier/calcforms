package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.DecentralizeDecisionMaking;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the DecentralizeDecisionMaking entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DecentralizeDecisionMakingRepository extends MongoRepository<DecentralizeDecisionMaking, String> {
}
