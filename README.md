# AGITELIER - FORMULAIRE DE CALCUL

## Description

## Auteur

Yannick Bellerose (BELY13079406), Serban-Costin Ivanescu(IVAS14069704), Mohamed Yahyani (YAHM14059203)

## Lancer App Web

1. Installer Node.js
2. Rouler dans le projet:
   ```
   npm install
   ```
3. Installer Docker (aller voir documention officiel sur leur site) (reboot peut etre necessaire pour que Docker roule bien)
4. Lancer
   ```
   docker-compose -f src/main/docker/mongodb.yml up -d
   docker-compose -f src/main/docker/keycloak.yml up -d
   ```
5. Lancer le front-end et back-end:
   ```
   npm start
   ./mvwn
   ```
6. Vous pouvez aussi utiliser le script shell pour faire 4 et 5:
   ```
   ./appLauncher.sh
   ```

P.S. L'installation de JHipster et pas necessaire pour faire rouler l'app parce que le projet est deja installer, mais il est possible qu'on aille besoin de l'utiliser plus tard alors mieux l'installer (ex: pour importer les .jdl, il faut JHipster).

## Fonctionnement

Pour utiliser le logiciel il faut d'abord se connecter comme utilisateur puis aller manuelement à l'adresse suivante: 

YOUR_HOST/weighted-shortest-job-first/YOUR_CLASS/YOUR_GROUP 

En général, quand vous travaillez en local, YOUR_HOST prend la valeur localhost:9000 ou localhost:8080 

Vous pouvez donnez la valeur de votre choix à la classe et au groupe 

## Contenu du projet

<décrivez brièvement chacun des fichiers contenus dans le projet (une phrase
par fichier)>

## Références

 <citez vos sources ici>

## Statut

<indiquez si le projet est complété ou s'il y a des bogues>
